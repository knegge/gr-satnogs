/* -*- c++ -*- */
/*
 *  gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2021-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef INCLUDED_SATNOGS_AX100_ENCODER_H
#define INCLUDED_SATNOGS_AX100_ENCODER_H

#include <gnuradio/satnogs/api.h>
#include <gnuradio/satnogs/crc.h>
#include <gnuradio/satnogs/encoder.h>
#include <gnuradio/satnogs/whitening.h>

#include <vector>

namespace gr {
namespace satnogs {
/*!
 * \brief AX.100 frame encoder definition
 * \ingroup satnogs
 *
 */
class SATNOGS_API ax100_encoder
{
public:
    static encoder::encoder_sptr mode5(const std::vector<uint8_t>& preamble,
                                       const std::vector<uint8_t>& sync,
                                       crc::type crc,
                                       whitening::sptr scrambler,
                                       bool enable_rs);

    static encoder::encoder_sptr mode6(size_t preamble_len,
                                       size_t postamble_len,
                                       whitening::sptr scrambler,
                                       bool ax25_scrambling,
                                       bool nrzi,
                                       crc::type crc);
};

} // namespace satnogs
} // namespace gr

#endif /* INCLUDED_SATNOGS_AX100_ENCODER_H */
