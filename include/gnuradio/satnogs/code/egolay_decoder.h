/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2022, 2024 Libre Space Foundation <http://libre.space/>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EGOLAY_DECODER_H
#define EGOLAY_DECODER_H

#include <gnuradio/fec/decoder.h>
#include <gnuradio/satnogs/api.h>
#include <gnuradio/satnogs/code/code.h>
#include <itpp/comm/egolay.h>

namespace gr {
namespace satnogs {
namespace code {

class SATNOGS_API egolay_decoder : virtual public fec::generic_decoder
{
public:
    static fec::generic_decoder::sptr make(bool packed = false);

    egolay_decoder(bool packed = false);

    void generic_work(fec_input_buffer_type inbuffer, void* outbuffer) override;

    /*!
     * Returns the rate of the code. For every r input bits, there
     * is 1 output bit, so the rate is 1/r. Used for setting things
     * like the encoder block's relative rate.
     *
     * This function MUST be reimplemented by the child class.
     */
    double rate() override;

    /*!
     * Returns the input size in items that the decoder object uses
     * to decode a full frame. Often, this number is the number of
     * bits per frame if the input format is unpacked. If the block
     * expects packed bytes, then this value should be the number of
     * bytes (number of bits / 8) per input frame.
     *
     * The child class MUST implement this function.
     */
    int get_input_size() override;

    /*!
     * Returns the output size in items that the decoder object
     * produces after decoding a full frame. Often, this number is
     * the number of bits in the outputted frame if the input format
     * is unpacked. If the block produces packed bytes, then this
     * value should be the number of bytes (number of bits / 8) per
     * frame produced. This value is generally something like
     * get_input_size()/R for a 1/R rate code.
     *
     * The child class MUST implement this function.
     */
    int get_output_size() override;

    /*!
     * Sets the size of an input item, as in the size of a char or
     * float item.
     *
     * The child class SHOULD implement this function. If not
     * reimplemented, it returns sizeof(float) as the decoders
     * typically expect floating point input types.
     */
    int get_input_item_size() override;

    /*!
     * Sets the size of an output item, as in the size of a char or
     * float item.
     *
     * The child class SHOULD implement this function. If not
     * reimplemented, it returns sizeof(char) as the decoders
     * typically expect to produce bits or bytes.
     */
    int get_output_item_size() override;

    /*!
     * Set up a conversion type required to setup the data properly
     * for this decoder. The decoder itself will not implement the
     * conversion and expects an external wrapper (e.g.,
     * fec.extended_decoder) to read this value and "do the right
     * thing" to format the data.
     *
     * The default behavior is 'none', which means no conversion is
     * required. Whatever the get_input_item_size() value returns,
     * the input is expected to conform directly to this.
     *
     * This may also return 'uchar', which indicates that the
     * wrapper should convert the standard float samples to unsigned
     * characters, either hard sliced or 8-bit soft symbols. See
     * gr::fec::code::cc_decoder as an example decoder that uses
     * this conversion format.
     *
     * If 'packed_bits', the block expects the inputs to be packed
     * hard bits. Each input item is a unsigned char where each of
     * the 8-bits is a hard bit value.
     *
     * The child class SHOULD implement this function. If not
     * reimplemented, it returns "none".
     */
    const char* get_input_conversion() override;

    /*!
     * Set up a conversion type required to understand the output
     * style of this decoder. Generally, follow-on processing
     * expects unpacked bits, so we specify the conversion type here
     * to indicate what the wrapper (e.g., fec.extended_decoder)
     * should do to convert the output samples from the decoder into
     * unpacked bits.
     *
     * The default behavior is 'none', which means no conversion is
     * required. This should mean that the output data is produced
     * from this decoder as unpacked bit.
     *
     * If 'unpack', the block produces packed bytes that should be
     * unpacked by the wrapper. See gr::fec::code::ccsds_decoder as
     * an example of a decoder that produces packed bytes.
     *
     * The child class SHOULD implement this function. If not
     * reimplemented, it returns "none".
     */
    const char* get_output_conversion() override;

    /*!
     * Updates the size of a decoded frame.
     *
     * The child class MUST implement this function and interpret
     * how the \p frame_size information affects the block's
     * behavior. It should also provide bounds checks.
     */
    bool set_frame_size(unsigned int frame_size) override;

private:
    const bool m_packed;
    size_t m_frame_len;


    void to_bvec(itpp::bvec& out, const uint8_t* in);
    void from_bvec(uint8_t* out, const itpp::bvec& in);
};


} // namespace code
} // namespace satnogs
} // namespace gr

#endif // EGOLAY_DECODER_H
