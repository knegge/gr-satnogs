/* -*- c++ -*- */
/*
 *  gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2021-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef INCLUDED_SATNOGS_AX100_MODE6_ENCODER_H
#define INCLUDED_SATNOGS_AX100_MODE6_ENCODER_H

#include <gnuradio/satnogs/api.h>
#include <gnuradio/satnogs/ax25_encoder.h>
#include <gnuradio/satnogs/crc.h>
#include <gnuradio/satnogs/encoder.h>
#include <gnuradio/satnogs/whitening.h>

namespace gr {
namespace satnogs {

/*!
 * \brief This encoder implements the AX100 mode 6 framing and coding scheme
 *
 */
class SATNOGS_API ax100_mode6_encoder : public encoder
{
public:
    using sptr = std::shared_ptr<ax100_mode6_encoder>;
    static sptr make(size_t preamble_len,
                     size_t postamble_len,
                     whitening::sptr scrambler,
                     bool scramble,
                     bool nrzi,
                     crc::type crc);

    ax100_mode6_encoder(size_t preamble_len,
                        size_t postamble_len,
                        whitening::sptr scrambler,
                        bool scramble,
                        bool nrzi,
                        crc::type crc);
    ~ax100_mode6_encoder();

    pmt::pmt_t encode(pmt::pmt_t msg);

private:
    const size_t d_max_frame_len;
    ax25_encoder d_ax25;
    whitening::sptr d_scrambler;
    crc::type d_crc;
    uint8_t* d_pdu;
};

} // namespace satnogs
} // namespace gr

#endif /* INCLUDED_SATNOGS_AX100_MODE5_ENCODER_H */
