/* -*- c++ -*- */
/*
 *  gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2019-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */


#ifndef INCLUDED_SATNOGS_CRC_H
#define INCLUDED_SATNOGS_CRC_H

#define CRCPP_INCLUDE_ESOTERIC_CRC_DEFINITIONS 1
#define CRCPP_USE_CPP11 1

#include <gnuradio/satnogs/api.h>
#include <CRC.h>
#include <cstddef>
#include <cstdint>
#include <stdexcept>

namespace gr {
namespace satnogs {

/*!
 * CRC class providing a range of different CRC calculation static methods
 *
 */
class SATNOGS_API crc
{
public:
    /**
     * @brief Predefined CRC types
     *
     * Names and alias retrieved from https://reveng.sourceforge.io/crc-catalogue/
     */
    enum class type {
        NONE = 0,
        CRC16_AUG_CCITT,
        CRC16_AUG_CCITT_XOR, //! XOR'd version of the CRC-16/AUG-CCITT
        CRC16_CMS,
        CRC16_HDLC, //! Alias: CRC-16/IBM-SDLC, CRC-16/ISO-HDLC, CRC-16/ISO-IEC-14443-3-B,
                    //! CRC-16/X-25, CRC-B, X-25
        CRC16_KERMIT, //! Alias: CRC-16/BLUETOOTH, CRC-16/CCITT, CRC-16/CCITT-TRUE,
                      //! CRC-16/V-41-LSB, CRC-CCITT, KERMIT
        CRC16_XMODEM, //! Alias: CRC-16/ACORN, CRC-16/LTE, CRC-16/V-41-MSB, XMODEM, ZMODEM
        CRC32_C,      //! Alias: CRC-32/BASE91-C, CRC-32/CASTAGNOLI, CRC-32/INTERLAKEN,
                      //! CRC-32/ISCSI

    };

    /**
     *
     * @param t the CRC method
     * @return the size of the specified CRC in bytes
     */
    static constexpr size_t size(type t)
    {
        switch (t) {
        case type::NONE:
            return 0;
        case type::CRC16_XMODEM:
        case type::CRC16_CMS:
        case type::CRC16_HDLC:
        case type::CRC16_AUG_CCITT:
        case type::CRC16_AUG_CCITT_XOR:
        case type::CRC16_KERMIT:
            return sizeof(uint16_t);
        case type::CRC32_C:
            return sizeof(uint32_t);
        default:
            throw std::invalid_argument("crc: Invalid CRC method");
        }
    }

    template <typename CRCType, crcpp_uint16 CRCWidth>
    static constexpr size_t size(const CRC::Parameters<CRCType, CRCWidth>& t)
    {
        static_assert(CRCWidth % 8 == 0, "Expected a CRC size with byte boundaries");
        return CRCWidth / 8;
    }

    template <typename CRCType, crcpp_uint16 CRCWidth>
    static size_t append(const CRC::Parameters<CRCType, CRCWidth>& t,
                         uint8_t* out,
                         const uint8_t* data,
                         size_t len,
                         bool msb = true)
    {
        static_assert(CRCWidth % 8 == 0, "Expected a CRC size with byte boundaries");
        auto calc = CRC::Calculate(data, len, t);
        if (msb) {
            for (int i = sizeof(calc) - 1; i >= 0; i--) {
                *out++ = calc >> (i * 8);
            }
        } else {
            for (size_t i = 0; i < sizeof(calc); i++) {
                *out++ = calc >> (i * 8);
            }
        }
        return sizeof(calc);
    }

    static size_t
    append(type t, uint8_t* out, const uint8_t* data, size_t len, bool msb = true);

    static bool check(type t, const uint8_t* data, size_t len, bool msb = true);

    template <typename CRCType, crcpp_uint16 CRCWidth>
    static bool check(const CRC::Parameters<CRCType, CRCWidth>& t,
                      const uint8_t* data,
                      size_t len,
                      bool msb = true)
    {
        const size_t payload_len = len - crc::size(t);
        auto calc = CRC::Calculate(data, payload_len, t);
        CRCType recv = 0x0;
        if (msb) {
            for (size_t i = 0; i < sizeof(recv); i++) {
                recv <<= 8;
                recv |= data[payload_len + i];
            }
        } else {
            for (int i = sizeof(recv) - 1; i >= 0; i--) {
                recv <<= 8;
                recv |= data[payload_len + i];
            }
        }
        return calc == recv;
    }
};

} // namespace satnogs
} // namespace gr

#endif /* INCLUDED_SATNOGS_CRC_H */
