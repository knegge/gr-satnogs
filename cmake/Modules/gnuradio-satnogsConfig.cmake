find_package(PkgConfig)

PKG_CHECK_MODULES(PC_GR_SATNOGS gnuradio-satnogs)

FIND_PATH(
    GR_SATNOGS_INCLUDE_DIRS
    NAMES gnuradio/satnogs/api.h
    HINTS $ENV{SATNOGS_DIR}/include
        ${PC_SATNOGS_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    GR_SATNOGS_LIBRARIES
    NAMES gnuradio-satnogs
    HINTS $ENV{SATNOGS_DIR}/lib
        ${PC_SATNOGS_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/gnuradio-satnogsTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GR_SATNOGS DEFAULT_MSG GR_SATNOGS_LIBRARIES GR_SATNOGS_INCLUDE_DIRS)
MARK_AS_ADVANCED(GR_SATNOGS_LIBRARIES GR_SATNOGS_INCLUDE_DIRS)
