/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2016,2019,2022 Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INCLUDED_SATNOGS_DOPPLER_CORRECTION_CC_IMPL_H
#define INCLUDED_SATNOGS_DOPPLER_CORRECTION_CC_IMPL_H

#include <gnuradio/blocks/rotator.h>
#include <gnuradio/satnogs/doppler_correction_cc.h>


namespace gr {
namespace satnogs {

class doppler_correction_cc_impl : public doppler_correction_cc
{
private:
    const double m_samp_rate;
    const double m_bb_offset;
    const std::string m_tag_name;
    const int m_update_period;
    double m_last_offset;
    doppler_correction::sptr m_doppler_src;
    blocks::rotator m_rot;
    int m_proccessed;

    void reset(pmt::pmt_t msg);

public:
    doppler_correction_cc_impl(doppler_correction::sptr doppler,
                               double sampling_rate,
                               double offset = 0.0,
                               size_t corrections_per_sec = 1000,
                               const std::string& doppler_tag_name = "doppler");

    ~doppler_correction_cc_impl();

    bool start() override;

    bool stop() override;

    // Where all the action really happens
    int work(int noutput_items,
             gr_vector_const_void_star& input_items,
             gr_vector_void_star& output_items) override;
};

} // namespace satnogs
} // namespace gr

#endif /* INCLUDED_SATNOGS_DOPPLER_CORRECTION_CC_IMPL_H */
