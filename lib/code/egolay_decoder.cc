/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2022, 2024 Libre Space Foundation <http://libre.space/>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gnuradio/logger.h>
#include <gnuradio/satnogs/code/egolay_decoder.h>

namespace gr::satnogs::code {

fec::generic_decoder::sptr egolay_decoder::make(bool packed)
{
    return fec::generic_decoder::sptr(new egolay_decoder(packed));
}

egolay_decoder::egolay_decoder(bool packed)
    : generic_decoder("egolay"), m_packed(packed), m_frame_len(0)
{
}

void egolay_decoder::generic_work(fec_input_buffer_type inbuffer, void* outbuffer)
{
    itpp::bvec coded;
    to_bvec(coded, static_cast<const uint8_t*>(inbuffer));
    auto egl = itpp::Extended_Golay();
    auto decoded = egl.decode(coded);
    from_bvec(static_cast<uint8_t*>(outbuffer), decoded);
}

double egolay_decoder::rate() { return 0.5; }

int egolay_decoder::get_input_size() { return m_frame_len * 2; }

int egolay_decoder::get_output_size() { return get_input_size() / 2; }

int egolay_decoder::get_input_item_size() { return sizeof(uint8_t); }

int egolay_decoder::get_output_item_size() { return sizeof(uint8_t); }

const char* egolay_decoder::get_input_conversion()
{
    return m_packed ? "packed_bits" : "none";
}

const char* egolay_decoder::get_output_conversion()
{
    return m_packed ? "unpack" : "none";
}

bool egolay_decoder::set_frame_size(unsigned int frame_size)
{
    if (m_packed) {
        if ((frame_size * 8) % 12) {
            d_logger->warn("tried to set a packed frame length of {}. The "
                           "encoder supports lengths that are multiple of 12 "
                           "bits",
                           frame_size);
            return false;
        }
    } else {
        if (frame_size % 12) {
            d_logger->warn("tried to set a unpacked frame length of {}. The "
                           "encoder supports lengths that are multiple of 12 "
                           "bits",
                           frame_size);
            return false;
        }
    }
    m_frame_len = frame_size;
    return true;
}


void egolay_decoder::to_bvec(itpp::bvec& out, const uint8_t* in)
{
    if (m_packed) {
        for (size_t i = 0; i < m_frame_len * 2; i++) {
            out = itpp::concat(out, itpp::bin(in[i] >> 7));
            out = itpp::concat(out, itpp::bin((in[i] >> 6) & 1));
            out = itpp::concat(out, itpp::bin((in[i] >> 5) & 1));
            out = itpp::concat(out, itpp::bin((in[i] >> 4) & 1));
            out = itpp::concat(out, itpp::bin((in[i] >> 3) & 1));
            out = itpp::concat(out, itpp::bin((in[i] >> 2) & 1));
            out = itpp::concat(out, itpp::bin((in[i] >> 1) & 1));
            out = itpp::concat(out, itpp::bin(in[i] & 1));
        }
    } else {
        for (size_t i = 0; i < m_frame_len * 2; i++) {
            out = itpp::concat(out, itpp::bin(in[i]));
        }
    }
}

void egolay_decoder::from_bvec(uint8_t* out, const itpp::bvec& in)
{
    if (m_packed) {
        for (int i; i < in.length();) {
            uint8_t c = in[i++].value() << 7;
            c |= in[i++].value() << 6;
            c |= in[i++].value() << 5;
            c |= in[i++].value() << 4;
            c |= in[i++].value() << 3;
            c |= in[i++].value() << 2;
            c |= in[i++].value() << 1;
            c |= in[i++].value();
            *out++ = c;
        }
    } else {
        for (int i; i < in.length(); i++) {
            *out++ = in[i].value();
        }
    }
}

} // namespace gr::satnogs::code
