/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2022, 2024 Libre Space Foundation <http://libre.space/>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gnuradio/fec/rs.h>
#include <gnuradio/satnogs/code/ccsds_rs_decoder.h>

namespace gr {
namespace satnogs {
namespace code {

fec::generic_decoder::sptr
ccsds_rs_decoder::make(ccsds_rs_encoder::ecc e,
                       ccsds_rs_encoder::interleaving_depth depth)
{
    return fec::generic_decoder::sptr(new ccsds_rs_decoder(e, depth));
}

ccsds_rs_decoder::ccsds_rs_decoder(ccsds_rs_encoder::ecc e,
                                   ccsds_rs_encoder::interleaving_depth depth)
    : fec::generic_decoder("CCSDS RS Decoder"),
      m_ecc(e),
      m_depth(depth),
      m_rs_parity(2 * static_cast<std::underlying_type_t<ccsds_rs_encoder::ecc>>(m_ecc)),
      m_max_in_frame_len(
          255 * static_cast<std::underlying_type_t<ccsds_rs_encoder::interleaving_depth>>(
                    m_depth)),
      m_max_out_frame_len(
          (255 - m_rs_parity) *
          static_cast<std::underlying_type_t<ccsds_rs_encoder::interleaving_depth>>(
              m_depth)),
      m_frame_len(0)
{
    if (m_ecc == ccsds_rs_encoder::ecc::ecc8) {
        d_logger->fatal("currently ECC8 is not supported");
        throw std::runtime_error(alias() + ": Currently ECC8 is not supported");
    }
    set_frame_size(m_max_in_frame_len);
}

extern "C" int decode_rs_8(unsigned char* data, int* eras_pos, int no_eras);

void ccsds_rs_decoder::generic_work(fec_input_buffer_type inbuffer, void* outbuffer)
{
    const size_t interleaving =
        static_cast<std::underlying_type_t<ccsds_rs_encoder::interleaving_depth>>(
            m_depth);
    /* Perform deinterleave and virtual fill */
    size_t vfill = 255 * interleaving - m_frame_len;
    const size_t ivfill = (vfill / interleaving);

    std::vector<std::vector<uint8_t>> buffers(interleaving, std::vector<uint8_t>(255, 0));
    const uint8_t* in = static_cast<const uint8_t*>(inbuffer);
    uint8_t* out = static_cast<uint8_t*>(outbuffer);

    for (size_t i = 0, s = 0; i < m_frame_len; i++) {
        buffers[s][ivfill + i / interleaving] = in[i];
        s = (s + 1) % interleaving;
    }

    for (auto& i : buffers) {
        int ret = decode_rs_8(i.data(), nullptr, 0);
        if (ret < 0) {
            d_logger->debug("dropping RS block due to error symbols");
            return;
        } else {
            d_logger->debug("decoded RS block with {} error symbols", ret);
        }
    }

    /* Compact the messages from each decoder */
    for (size_t i = 0, s = 0; i < m_frame_len - m_rs_parity * interleaving; i++) {
        out[i] = buffers[s][ivfill + i / interleaving];
        s = (s + 1) % interleaving;
    }
}

double ccsds_rs_decoder::rate()
{
    return m_ecc == ccsds_rs_encoder::ecc::ecc8 ? 239.0 / 255.0 : 223.0 / 255.0;
}

int ccsds_rs_decoder::get_input_size() { return m_max_in_frame_len; }

int ccsds_rs_decoder::get_output_size() { return m_max_out_frame_len; }

int ccsds_rs_decoder::get_input_item_size() { return sizeof(char); };

int ccsds_rs_decoder::get_output_item_size() { return sizeof(char); }

const char* ccsds_rs_decoder::get_input_conversion() { return "packed_bits"; }

const char* ccsds_rs_decoder::get_output_conversion() { return "none"; }

bool ccsds_rs_decoder::set_frame_size(unsigned int frame_size)
{
    if (frame_size > m_max_in_frame_len) {
        d_logger->info("tried to set frame to %1%; max possible is {}",
                       frame_size,
                       m_max_in_frame_len);
        return false;
    }
    m_frame_len = frame_size;
    return true;
}

} // namespace code
} // namespace satnogs
} // namespace gr
