/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2022, 2024 Libre Space Foundation <http://libre.space/>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gnuradio/fec/rs.h>
#include <gnuradio/satnogs/code/ccsds_rs_encoder.h>
#include <type_traits>

namespace gr {
namespace satnogs {
namespace code {

fec::generic_encoder::sptr ccsds_rs_encoder::make(ecc e, interleaving_depth depth)
{
    return generic_encoder::sptr(new ccsds_rs_encoder(e, depth));
}


ccsds_rs_encoder::ccsds_rs_encoder(ecc e, interleaving_depth depth)
    : fec::generic_encoder("CCSDS RS Encoder"),
      m_ecc(e),
      m_depth(depth),
      m_rs_parity(2 * static_cast<std::underlying_type_t<ecc>>(m_ecc)),
      m_max_in_frame_len(
          (255 - m_rs_parity) *
          static_cast<std::underlying_type_t<interleaving_depth>>(m_depth)),
      m_max_out_frame_len(
          255 * static_cast<std::underlying_type_t<interleaving_depth>>(m_depth)),
      m_frame_len(0)
{
    if (m_ecc == ecc::ecc8) {
        d_logger->fatal("Currently ECC8 is not supported");
        throw std::runtime_error("Currently ECC8 is not supported");
    }
    set_frame_size(m_max_in_frame_len);
}

/*!
 * Returns the coding rate of this encoder. Due to virtual fill this method returns
 * the lowest possible coding rate
 */
double ccsds_rs_encoder::rate()
{
    return m_ecc == ecc::ecc8 ? 239.0 / 255.0 : 223.0 / 255.0;
}

int ccsds_rs_encoder::get_input_size() { return m_max_in_frame_len; }

int ccsds_rs_encoder::get_output_size() { return m_max_out_frame_len; }

const char* ccsds_rs_encoder::get_input_conversion() { return "packed"; }

const char* ccsds_rs_encoder::get_output_conversion() { return "packed_bits"; }

bool ccsds_rs_encoder::set_frame_size(unsigned int frame_size)
{
    if (frame_size > m_max_in_frame_len) {
        d_logger->info("tried to set frame to {}; max possible is {}",
                       frame_size,
                       m_max_in_frame_len);
        return false;
    }
    m_frame_len = frame_size;
    return true;
}

extern "C" void encode_rs_8(unsigned char* data, unsigned char* parity);

void ccsds_rs_encoder::generic_work(fec_input_buffer_type inbuffer, void* outbuffer)
{
    const size_t interleaving =
        static_cast<std::underlying_type_t<interleaving_depth>>(m_depth);
    size_t total_vfill = m_max_out_frame_len - m_frame_len - m_rs_parity * interleaving;

    /* Virtual fill should fill entirely on all interleaved blocks */
    if (total_vfill % interleaving) {
        d_logger->fatal("Invalid combination of frame size and settings");
        throw std::runtime_error(alias() +
                                 ": Invalid combination of frame size and settings");
    }

    size_t vfill = (total_vfill / interleaving);
    std::vector<std::vector<uint8_t>> buffers(interleaving, std::vector<uint8_t>(255, 0));

    const uint8_t* in = static_cast<const uint8_t*>(inbuffer);
    uint8_t* out = static_cast<uint8_t*>(outbuffer);
    for (size_t i = 0, s = 0; i < m_frame_len; i++) {
        buffers[s][vfill + i / interleaving] = in[i];
        s = (s + 1) % interleaving;
    }

    /* Encode the interleaved bytes */
    for (auto& i : buffers) {
        encode_rs_8(i.data(), i.data() + (255 - m_rs_parity));
    }

    /* Repack messages from each encoder */
    for (size_t i = 0, s = 0; i < m_frame_len + interleaving * m_rs_parity; i++) {
        out[i] = buffers[s][vfill + i / interleaving];
        s = (s + 1) % interleaving;
    }
}

} // namespace code
} // namespace satnogs
} // namespace gr
