/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gnuradio/satnogs/rigctl_doppler_correction.h>

namespace gr {
namespace satnogs {

/**
 * @brief Creates a shared pointer to a rigctl doppler correction object
 *
 * @param center_freq the center frequency
 * @param model the rig model
 * @param vfo the target VFO (can be changed on demand with the set_vfo())
 * @return doppler_correction::sptr
 */
doppler_correction::sptr
rigctl_doppler_correction::make(double center_freq, rig_model_t model, vfo_t vfo)
{
    return doppler_correction::sptr(
        new rigctl_doppler_correction(center_freq, model, vfo));
}

void rigctl_doppler_correction::start()
{
    m_rig.open();
    set_vfo(m_vfo);
}

void rigctl_doppler_correction::stop() { m_rig.close(); }

double rigctl_doppler_correction::offset() { return m_freq - m_rig.getFreq(); }

void rigctl_doppler_correction::reset() {}

/**
 * @brief Sets the active VFO of the rig
 *
 * @param vfo
 */
void rigctl_doppler_correction::set_vfo(vfo_t vfo)
{
    try {
        m_rig.setVFO(vfo);
        m_vfo = vfo;
    } catch (RigException& e) {
        e.print();
    }
}

/**
 * @brief Sets a parameter for the rig
 *
 * @param name the name of the parameter
 * @param val  the value of the parameter
 */
void rigctl_doppler_correction::set_param(const std::string& name, const std::string& val)
{
    m_rig.setConf(name.c_str(), val.c_str());
}

/**
 * @brief Construct a new rigctl doppler correction::rigctl doppler correction object
 *
 * @param center_freq the center frequency that the target object is operating at
 * @param model the rig model
 * @param vfo the target VFO (can be changed on demand with the set_vfo())
 */
rigctl_doppler_correction::rigctl_doppler_correction(double center_freq,
                                                     rig_model_t model,
                                                     vfo_t vfo)
    : doppler_correction("rigctl"), m_freq(center_freq), m_vfo(vfo), m_rig(model)
{
    rig_set_debug(RIG_DEBUG_ERR);
}


} // namespace satnogs
} // namespace gr