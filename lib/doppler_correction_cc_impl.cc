/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2016,2019,2022 Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "doppler_correction_cc_impl.h"
#include <gnuradio/io_signature.h>
#include <volk/volk.h>

namespace gr {
namespace satnogs {

doppler_correction_cc::sptr
doppler_correction_cc::make(doppler_correction::sptr doppler,
                            double sampling_rate,
                            double offset,
                            size_t corrections_per_sec,
                            const std::string& doppler_tag_name)
{
    return doppler_correction_cc::sptr(new doppler_correction_cc_impl(
        doppler, sampling_rate, offset, corrections_per_sec, doppler_tag_name));
}

/*
 * The private constructor
 */
doppler_correction_cc_impl::doppler_correction_cc_impl(
    doppler_correction::sptr doppler,
    double sampling_rate,
    double offset,
    size_t corrections_per_sec,
    const std::string& doppler_tag_name)
    : gr::sync_block("doppler_correction_cc",
                     gr::io_signature::make(1, 1, sizeof(gr_complex)),
                     gr::io_signature::make(1, 1, sizeof(gr_complex))),
      m_samp_rate(sampling_rate),
      m_bb_offset(offset),
      m_tag_name(doppler_tag_name),
      m_update_period(sampling_rate / corrections_per_sec),
      m_last_offset(0),
      m_doppler_src(doppler),
      m_proccessed(0)
{
    message_port_register_in(pmt::mp("reset"));
    set_msg_handler(pmt::mp("reset"), [this](pmt::pmt_t msg) { this->reset(msg); });
    m_rot.set_phase_incr(std::exp(
        gr_complex(0.0, (2 * M_PI * (-m_last_offset - m_bb_offset)) / m_samp_rate)));
}

void doppler_correction_cc_impl::reset(pmt::pmt_t msg) { m_doppler_src->reset(); }

bool doppler_correction_cc_impl::start()
{
    m_doppler_src->start();
    m_last_offset = m_doppler_src->offset();
    return true;
}
bool doppler_correction_cc_impl::stop()
{
    m_doppler_src->stop();
    return true;
}

/*
 * Our virtual destructor.
 */
doppler_correction_cc_impl::~doppler_correction_cc_impl() {}

int doppler_correction_cc_impl::work(int noutput_items,
                                     gr_vector_const_void_star& input_items,
                                     gr_vector_void_star& output_items)
{
    const gr_complex* in = static_cast<const gr_complex*>(input_items[0]);
    gr_complex* out = static_cast<gr_complex*>(output_items[0]);
    int cnt = std::min(m_update_period - m_proccessed, noutput_items);
    m_rot.rotateN(out, in, cnt);

    const int wins = (noutput_items - cnt) / m_update_period;
    for (int i = 0; i < wins; i++) {
        double offset = m_doppler_src->offset();
        if (std::fabs(offset - m_last_offset) > 1) {
            m_last_offset = offset;
            this->add_item_tag(0,
                               this->nitems_written(0) + cnt,
                               pmt::mp(m_tag_name),
                               pmt::from_double(offset),
                               this->alias_pmt());
        }
        m_rot.set_phase_incr(std::exp(
            gr_complex(0.0, (2 * M_PI * (-offset - m_bb_offset)) / m_samp_rate)));
        m_rot.rotateN(out + cnt, in + cnt, m_update_period);
        cnt += m_update_period;
    }

    /* Proccess any left over samples */
    m_proccessed = noutput_items - cnt;
    double offset = m_doppler_src->offset();
    if (std::fabs(offset - m_last_offset) > 1) {
        m_last_offset = offset;
        this->add_item_tag(0,
                           this->nitems_written(0) + cnt,
                           pmt::mp(m_tag_name),
                           pmt::from_double(offset),
                           this->alias_pmt());
    }
    m_rot.set_phase_incr(
        std::exp(gr_complex(0.0, (2 * M_PI * (-offset - m_bb_offset)) / m_samp_rate)));
    m_rot.rotateN(out + cnt, in + cnt, m_proccessed);
    return noutput_items;
}

} /* namespace satnogs */
} /* namespace gr */
