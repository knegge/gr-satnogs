/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2022 Libre Space Foundation <http://libre.space/>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gnuradio/attributes.h>
#include <gnuradio/satnogs/code/egolay_decoder.h>
#include <gnuradio/satnogs/code/egolay_encoder.h>
#include <unordered_set>
#include <boost/test/unit_test.hpp>
#include <random>

namespace gr {
namespace satnogs {
namespace code {

BOOST_AUTO_TEST_CASE(simple_packed)
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<uint8_t> uni(0, 255);
    auto gen = std::bind(uni, mt);

    std::vector<uint8_t> data_in(3, 0);
    std::generate_n(data_in.begin(), 3, gen);
    std::vector<uint8_t> data_coded(3 * 2, 0);
    std::vector<uint8_t> data_decoded(3, 0);

    auto enc = egolay_encoder::make(true);
    auto dec = egolay_decoder::make(true);
    enc->set_frame_size(3);
    dec->set_frame_size(3);
    enc->generic_work(data_in.data(), data_coded.data());
    dec->generic_work(data_coded.data(), data_decoded.data());
    BOOST_REQUIRE_EQUAL(data_in == data_decoded, true);
}

BOOST_AUTO_TEST_CASE(simple_unpacked)
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<uint8_t> uni(0, 1);
    auto gen = std::bind(uni, mt);

    std::vector<uint8_t> data_in(12, 0);
    std::generate_n(data_in.begin(), 12, gen);
    std::vector<uint8_t> data_coded(12 * 2, 0);
    std::vector<uint8_t> data_decoded(12, 0);

    auto enc = egolay_encoder::make();
    auto dec = egolay_decoder::make();
    enc->set_frame_size(12);
    dec->set_frame_size(12);
    enc->generic_work(data_in.data(), data_coded.data());
    dec->generic_work(data_coded.data(), data_decoded.data());
    BOOST_REQUIRE_EQUAL(data_in == data_decoded, true);
}

BOOST_AUTO_TEST_CASE(max_errors_simple_packed)
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<uint8_t> uni(0, 255);
    auto gen = std::bind(uni, mt);

    std::vector<uint8_t> data_in(3, 0);
    std::generate_n(data_in.begin(), 3, gen);
    std::vector<uint8_t> data_coded(3 * 2, 0);
    std::vector<uint8_t> data_decoded(3, 0);

    auto enc = egolay_encoder::make(true);
    auto dec = egolay_decoder::make(true);
    enc->set_frame_size(3);
    dec->set_frame_size(3);
    enc->generic_work(data_in.data(), data_coded.data());
    data_coded[0] ^= 7; // 0b00000111
    data_coded[3] ^= 7;

    dec->generic_work(data_coded.data(), data_decoded.data());
    BOOST_REQUIRE_EQUAL(data_in == data_decoded, true);
}

BOOST_AUTO_TEST_CASE(max_errors_simple_unpacked)
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<uint8_t> uni(0, 1);
    std::uniform_int_distribution<uint8_t> uni_idx(0, 23);
    auto gen = std::bind(uni, mt);

    std::vector<uint8_t> data_in(12, 0);
    std::generate_n(data_in.begin(), 12, gen);
    std::vector<uint8_t> data_coded(12 * 2, 0);
    std::vector<uint8_t> data_decoded(12, 0);

    auto enc = egolay_encoder::make();
    auto dec = egolay_decoder::make();
    enc->set_frame_size(12);
    dec->set_frame_size(12);
    enc->generic_work(data_in.data(), data_coded.data());

    std::unordered_set<uint8_t> idx;
    while (idx.size() < 3) {
        idx.insert(uni_idx(mt));
    }
    for (auto elem : idx) {
        data_coded[elem] ^= 1; // 0b00000001
    }

    dec->generic_work(data_coded.data(), data_decoded.data());
    BOOST_REQUIRE_EQUAL(data_in == data_decoded, true);
}


} // namespace code
} // namespace satnogs
} // namespace gr
