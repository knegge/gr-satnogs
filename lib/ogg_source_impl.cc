/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2017,2022 Libre Space Foundation <http://libre.space/>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <volk/volk.h>
#include <vorbis/codec.h>
#include <vorbis/vorbisfile.h>

#include "ogg_source_impl.h"

namespace gr {
namespace satnogs {

ogg_source::sptr ogg_source::make(const std::string& filename, int channels, bool repeat)
{
    return ogg_source::sptr(new ogg_source_impl(filename, channels, repeat));
}

/*
 * The private constructor
 */
ogg_source_impl::ogg_source_impl(const std::string& filename, int channels, bool repeat)
    : gr::sync_block("ogg_source",
                     gr::io_signature::make(0, 0, 0),
                     gr::io_signature::make(channels, channels, sizeof(float))),
      m_channels(channels),
      m_repeat(repeat),
      m_in_buffer(pcm_buffer_size, 0),
      m_out_buffer(pcm_buffer_size, 0)
{
    if (channels < 1) {
        d_logger->crit("At least one output channels should be specified");
        throw std::invalid_argument("At least one output channels should"
                                    " be specified");
    }

    if (ov_fopen(filename.c_str(), &m_ogvorb_f) < 0) {
        d_logger->crit("Could not open {}", filename);
        throw std::invalid_argument((fmt::format("Could not open {}", filename)));
    }

    vorbis_info* vi = ov_info(&m_ogvorb_f, -1);
    if (vi->channels != channels) {
        d_logger->crit("Channels number specified ({}) does not match the "
                       "channels of the ogg stream ({})",
                       channels,
                       vi->channels);
    }

    const int alignment_multiple = volk_get_alignment() / sizeof(float);
    set_alignment(std::max(1, alignment_multiple));
    set_max_noutput_items(pcm_buffer_size);
}

/*
 * Our virtual destructor.
 */
ogg_source_impl::~ogg_source_impl() { ov_clear(&m_ogvorb_f); }

int ogg_source_impl::work(int noutput_items,
                          gr_vector_const_void_star& input_items,
                          gr_vector_void_star& output_items)
{
    long int ret;
    int section = 0;
    int available = (noutput_items / m_channels);
    int available_samples = 0;
    int produced = 0;

    ret = ov_read(&m_ogvorb_f,
                  reinterpret_cast<char*>(m_in_buffer.data()),
                  available * sizeof(int16_t),
                  0,
                  sizeof(int16_t),
                  1,
                  &section);
    if (ret <= 0) {
        /*
         * If return value is EOF and the repeat mode is set seek back to the
         * start of the ogg stream
         */
        if (ret == 0 && m_repeat) {
            if (ov_seekable(&m_ogvorb_f)) {
                ov_time_seek(&m_ogvorb_f, 0);
                return 0;
            }
            d_logger->warn("File is not seakable");
        }
        return WORK_DONE;
    }

    available_samples = ret / sizeof(int16_t);
    /* Convert to float the signed-short audio samples */
    volk_16i_s32f_convert_32f(
        m_out_buffer.data(), m_in_buffer.data(), 1 << 15, available_samples);

    /* De-interleave the available channels */

    for (int i = 0; i < available_samples; i += m_channels, produced++) {
        for (int chan = 0; chan < m_channels; chan++) {
            auto out = static_cast<float*>(output_items[chan]);
            out[produced] = m_out_buffer[i * m_channels + chan];
        }
    }

    return produced;
}

} /* namespace satnogs */
} /* namespace gr */
