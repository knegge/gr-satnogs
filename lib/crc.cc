/* -*- c++ -*- */
/*
 *  gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2019-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */


#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/satnogs/crc.h>
#include <CRC.h>

namespace gr {
namespace satnogs {


/**
 * Appends a CRC at the end of the buffer
 * @param t the CRC type
 * @param out the output buffer
 * @param data the input buffer
 * @param len the size of the input buffer in bytes
 * @param msb if set to true, the CRC will be appended starting from the most significant
 * byte
 * @return the number of bytes appended
 */
size_t crc::append(type t, uint8_t* out, const uint8_t* data, size_t len, bool msb)
{
    switch (t) {
    case type::NONE:
        return 0;
    case type::CRC16_XMODEM:
        return append(CRC::CRC_16_XMODEM(), out, data, len, msb);
    case type::CRC16_CMS:
        return append(CRC::CRC_16_CMS(), out, data, len, msb);
    case type::CRC16_HDLC:
        return append(CRC::CRC_16_X25(), out, data, len, msb);
    case type::CRC16_AUG_CCITT:
        return append(
            (CRC::Parameters<crcpp_uint16, 16>){ 0x1021, 0x1d0f, 0x0000, false, false },
            out,
            data,
            len,
            msb);
    case type::CRC16_AUG_CCITT_XOR:
        return append(
            (CRC::Parameters<crcpp_uint16, 16>){ 0x1021, 0x1d0f, 0xffff, false, false },
            out,
            data,
            len,
            msb);
    case type::CRC16_KERMIT:
        return append(CRC::CRC_16_KERMIT(), out, data, len, msb);
    case type::CRC32_C:
        return append(CRC::CRC_32_C(), out, data, len, msb);
    default:
        throw std::invalid_argument("crc: Invalid CRC method");
    }
    return 0;
}


/**
 * Checks for the validity of the buffer containing at its end a CRC.
 *
 * @param t the CRC type
 * @param data the buffer containing the data and the CRC bytes at the end
 * @param len the length of the buffer including the CRC bytes
 * @param msb if set to true, the CRC is considered that have been sent starting from the
 * most significant byte
 * @return true if the CRC is correct, false otherwise
 */
bool crc::check(type t, const uint8_t* data, size_t len, bool msb)
{
    switch (t) {
    case type::NONE:
        return true;
    case type::CRC16_XMODEM:
        return check(CRC::CRC_16_XMODEM(), data, len, msb);
    case type::CRC16_CMS:
        return check(CRC::CRC_16_CMS(), data, len, msb);
    case type::CRC16_HDLC:
        return check(CRC::CRC_16_X25(), data, len, msb);
    case type::CRC16_AUG_CCITT:
        return check(
            (CRC::Parameters<crcpp_uint16, 16>){ 0x1021, 0x1d0f, 0x0000, false, false },
            data,
            len,
            msb);
    case type::CRC16_AUG_CCITT_XOR:
        return check(
            (CRC::Parameters<crcpp_uint16, 16>){ 0x1021, 0x1d0f, 0xffff, false, false },
            data,
            len,
            msb);
    case type::CRC16_KERMIT:
        return check(CRC::CRC_16_KERMIT(), data, len, msb);
    case type::CRC32_C:
        return check(CRC::CRC_32_C(), data, len, msb);
    default:
        throw std::invalid_argument("crc: Invalid CRC method");
    }
    return false;
}

} /* namespace satnogs */
} /* namespace gr */
