/* -*- c++ -*- */
/*
 *  gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2021-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/satnogs/ax100_encoder.h>
#include <gnuradio/satnogs/ax100_mode5_encoder.h>
#include <gnuradio/satnogs/ax100_mode6_encoder.h>

namespace gr {
namespace satnogs {

/**
 * @brief Creates a AX.100 Mode 5 encoder
 *
 * @param preamble the preamble to be used. Can be empty
 * @param sync the synchronization word
 * @param crc the CRC to be used
 * @param scrambler the scrambler to be used for the payload section, the CRC and the
 * RS parity
 * @param enable_rs set to true to append RS parity bytes
 * @return encoder::encoder_sptr shared pointer to the encoder object
 */
encoder::encoder_sptr ax100_encoder::mode5(const std::vector<uint8_t>& preamble,
                                           const std::vector<uint8_t>& sync,
                                           crc::type crc,
                                           whitening::sptr scrambler,
                                           bool enable_rs)
{
    return ax100_mode5_encoder::make(preamble, sync, crc, scrambler, enable_rs);
}

/**
 * @brief Creates a AX.100 Mode 5 encoder
 *
 * @param preamble_len the number of repetitions of the AX.25 SYNC flag at the start of
 * the frame
 * @param postamble_len the number of repetitions of the AX.25 SYNC flag at the end of the
 * frame
 * @param scrambler the scrambler that will scramble the payload <b>before</b> the AX.25
 * frame
 * @param ax25_scrambling if true, AX.25 will apply its own scrambling
 * @param nrzi if true output will be NRZI encoded
 * @param crc the CRC to be appended at the end of the payload. This CRC does not affect
 * in any way the AX.25 CRC
 * @return encoder::encoder_sptr shared pointer to the encoder object
 */
encoder::encoder_sptr ax100_encoder::mode6(size_t preamble_len,
                                           size_t postamble_len,
                                           whitening::sptr scrambler,
                                           bool ax25_scrambling,
                                           bool nrzi,
                                           crc::type crc)
{
    return ax100_mode6_encoder::make(
        preamble_len, postamble_len, scrambler, ax25_scrambling, nrzi, crc);
}

} /* namespace satnogs */
} /* namespace gr */
