/*
 * Copyright 2023 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "pydoc_macros.h"
#define D(...) DOC(gr, satnogs, __VA_ARGS__)
/*
  This file contains placeholders for docstrings for the Python bindings.
  Do not edit! These were automatically extracted during the binding process
  and will be overwritten during the build process
 */


static const char* __doc_gr_satnogs_amsat_duv_decoder = R"doc()doc";


static const char* __doc_gr_satnogs_amsat_duv_decoder_amsat_duv_decoder_0 = R"doc()doc";


static const char* __doc_gr_satnogs_amsat_duv_decoder_amsat_duv_decoder_1 = R"doc()doc";


static const char* __doc_gr_satnogs_amsat_duv_decoder_make = R"doc()doc";


static const char* __doc_gr_satnogs_amsat_duv_decoder_decode = R"doc()doc";


static const char* __doc_gr_satnogs_amsat_duv_decoder_reset = R"doc()doc";
