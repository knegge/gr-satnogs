/*
 * Copyright 2020 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

#include <pybind11/pybind11.h>

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <gnuradio/satnogs/decoder.h>
#include <gnuradio/satnogs/encoder.h>
#include <numpy/arrayobject.h>

namespace py = pybind11;

// Headers for binding functions
/**************************************/
// The following comment block is used for
// gr_modtool to insert function prototypes
// Please do not delete
/**************************************/
// BINDING_FUNCTION_PROTOTYPES(

void bind_amsat_duv_decoder(py::module& m);
void bind_argos_ldr_decoder(py::module& m);
void bind_ax25_decoder(py::module& m);
void bind_ax25_encoder(py::module& m);
void bind_ax100_decoder(py::module& m);
void bind_ax100_encoder(py::module& m);
void bind_ber_calculator(py::module& m);
void bind_crc_async(py::module& m);
void bind_crc(py::module& m);
void bind_cw_decoder(py::module& m);
void bind_cw_encoder(py::module& m);
void bind_doppler_correction_cc(py::module& m);
void bind_doppler_correction(py::module& m);
void bind_dummy_doppler_correction(py::module& m);
void bind_frame_decoder(py::module& m);
void bind_frame_encoder(py::module& m);
void bind_ieee802_15_4_variant_decoder(py::module& m);
void bind_ieee802_15_4_encoder(py::module& m);
void bind_json_converter(py::module& m);
void bind_metadata(py::module& m);
void bind_metadata_sink(py::module& m);
void bind_noaa_apt_sink(py::module& m);
void bind_ogg_encoder(py::module& m);
void bind_ogg_source(py::module& m);
void bind_rigctl_doppler_correction(py::module& m);
void bind_sigmf_metadata(py::module& m);
void bind_sstv_pd120_sink(py::module& m);
void bind_waterfall_sink(py::module& m);
void bind_whitening(py::module& m);
// ) END BINDING_FUNCTION_PROTOTYPES


// We need this hack because import_array() returns NULL
// for newer Python versions.
// This function is also necessary because it ensures access to the C API
// and removes a warning.
void* init_numpy()
{
    import_array();
    return NULL;
}

PYBIND11_MODULE(satnogs_python, m)
{
    // Initialize the numpy C API
    // (otherwise we will see segmentation faults)
    init_numpy();

    // Allow access to base block methods
    py::module::import("gnuradio.gr");

    /**************************************/
    // The following comment block is used for
    // gr_modtool to insert binding function calls
    // Please do not delete
    /**************************************/
    // BINDING_FUNCTION_CALLS(

    py::class_<gr::satnogs::decoder, std::shared_ptr<gr::satnogs::decoder>>(m, "decoder");
    py::class_<gr::satnogs::encoder, std::shared_ptr<gr::satnogs::encoder>>(m, "encoder");
    bind_amsat_duv_decoder(m);
    bind_argos_ldr_decoder(m);
    bind_ax25_decoder(m);
    bind_ax25_encoder(m);
    bind_ax100_decoder(m);
    bind_ax100_encoder(m);
    bind_ber_calculator(m);
    bind_crc_async(m);
    bind_crc(m);
    bind_cw_decoder(m);
    bind_cw_encoder(m);
    bind_doppler_correction_cc(m);
    bind_doppler_correction(m);
    bind_dummy_doppler_correction(m);
    bind_frame_decoder(m);
    bind_frame_encoder(m);
    bind_ieee802_15_4_variant_decoder(m);
    bind_ieee802_15_4_encoder(m);
    bind_json_converter(m);
    bind_metadata_sink(m);
    bind_noaa_apt_sink(m);
    bind_ogg_encoder(m);
    bind_ogg_source(m);
    bind_rigctl_doppler_correction(m);
    bind_sigmf_metadata(m);
    bind_sstv_pd120_sink(m);
    bind_waterfall_sink(m);
    bind_whitening(m);
    // ) END BINDING_FUNCTION_CALLS
}
