/*
 * Copyright 2023 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

/***********************************************************************************/
/* This file is automatically generated using bindtool and can be manually edited  */
/* The following lines can be configured to regenerate this file during cmake      */
/* If manual edits are made, the following tags should be modified accordingly.    */
/* BINDTOOL_GEN_AUTOMATIC(0)                                                       */
/* BINDTOOL_USE_PYGCCXML(0)                                                        */
/* BINDTOOL_HEADER_FILE(argos_ldr_decoder.h)                                       */
/* BINDTOOL_HEADER_FILE_HASH(0)                                                    */
/***********************************************************************************/

#include <pybind11/complex.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

#include <gnuradio/satnogs/argos_ldr_decoder.h>
// pydoc.h is automatically generated in the build directory
#include <argos_ldr_decoder_pydoc.h>

void bind_argos_ldr_decoder(py::module& m)
{

    using argos_ldr_decoder = ::gr::satnogs::argos_ldr_decoder;


    py::class_<argos_ldr_decoder,
               gr::satnogs::decoder,
               std::shared_ptr<argos_ldr_decoder>>(
        m, "argos_ldr_decoder", D(argos_ldr_decoder))

        .def(py::init(&argos_ldr_decoder::make),
             py::arg("crc_check") = true,
             py::arg("max_frame_len") = 64,
             D(argos_ldr_decoder, make))


        .def("decode",
             &argos_ldr_decoder::decode,
             py::arg("in"),
             py::arg("len"),
             D(argos_ldr_decoder, decode))


        .def("reset", &argos_ldr_decoder::reset, D(argos_ldr_decoder, reset))

        ;
}
