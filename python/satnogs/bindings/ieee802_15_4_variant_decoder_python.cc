/*
 * Copyright 2023 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

/***********************************************************************************/
/* This file is automatically generated using bindtool and can be manually edited  */
/* The following lines can be configured to regenerate this file during cmake      */
/* If manual edits are made, the following tags should be modified accordingly.    */
/* BINDTOOL_GEN_AUTOMATIC(0)                                                       */
/* BINDTOOL_USE_PYGCCXML(0)                                                        */
/* BINDTOOL_HEADER_FILE(ieee802_15_4_variant_decoder.h)                            */
/* BINDTOOL_HEADER_FILE_HASH(0)                                                    */
/***********************************************************************************/

#include <pybind11/complex.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

#include <gnuradio/satnogs/decoder.h>
#include <gnuradio/satnogs/ieee802_15_4_variant_decoder.h>
// pydoc.h is automatically generated in the build directory
#include <ieee802_15_4_variant_decoder_pydoc.h>

void bind_ieee802_15_4_variant_decoder(py::module& m)
{

    using ieee802_15_4_variant_decoder = ::gr::satnogs::ieee802_15_4_variant_decoder;
    using decoder = ::gr::satnogs::decoder;

    py::class_<ieee802_15_4_variant_decoder,
               gr::satnogs::decoder,
               std::shared_ptr<ieee802_15_4_variant_decoder>>(
        m, "ieee802_15_4_variant_decoder", D(ieee802_15_4_variant_decoder))

        .def(py::init(&ieee802_15_4_variant_decoder::make),
             py::arg("preamble"),
             py::arg("preamble_threshold"),
             py::arg("sync"),
             py::arg("sync_threshold"),
             py::arg("crc"),
             py::arg("descrambler"),
             py::arg("var_len") = true,
             py::arg("max_len") = 1024,
             py::arg("drop_invalid") = true,
             py::arg("rs") = false,
             D(ieee802_15_4_variant_decoder, make))


        .def("decode",
             &ieee802_15_4_variant_decoder::decode,
             py::arg("in"),
             py::arg("len"),
             D(ieee802_15_4_variant_decoder, decode))


        .def("reset",
             &ieee802_15_4_variant_decoder::reset,
             D(ieee802_15_4_variant_decoder, reset))


        .def("input_multiple",
             &ieee802_15_4_variant_decoder::input_multiple,
             D(ieee802_15_4_variant_decoder, input_multiple))

        ;
}
